MITOCW | watch?v=ytpJdnlu9ug
The following content is provided under a Creative Commons license. Your support will help
MIT OpenCourseWare continue to offer high-quality, educational resources for free. To make
a donation, or view additional materials from hundreds of MIT courses, visit MIT
OpenCourseWare at ocw.mit.edu.
ANA BELL: All right. Let's begin. As I mentioned before, this lecture will be recorded for OCW. Again, in
future lectures, if you don't want to have the back of your head show up, just don't sit in this
front area here.
First of all, wow, what a crowd, you guys. We're finally in 26-100. 6.0001 made it big, huh?
Good afternoon and welcome to the very first class of 6.0001, and also 600, this semester.
My name is Ana Bell. First name, Ana. Last name, Bell. I'm a lecturer in the EECS Department.
And I'll be giving some of the lectures for today, along with later on in the term, Professor Eric
Grimson, who's sitting right down there, will be giving some of the lectures, as well.
Today we're going to go over some basic administrivia, a little bit of course information. And
then, we're going to talk a little bit about what is computation? We'll discuss at a very high level
what computers do just to make sure we're all on the same page.
And then, we're going to dive right into Python basics. We're going to talk a little bit about
mathematical operations you can do with Python. And then, we're going to talk about Python
variables and types.
As I mentioned in my introductory email, all the slides and code that I'll talk about during
lectures will be up before lecture, so I highly encourage you to download them and to have
them open. We're going to go through some in-class exercises which will be available on those
slides. And it's fun to do.
And it's also great if could take notes about the code just for future reference. It's true. This is
a really fast-paced course, and we ramp up really quickly. We do want to position you to
succeed in this course.
As I was writing this, I was trying to think about when I was first starting to program what
helped me get through my very first programming course. And this is really a good list. The
first thing was I just read the psets as soon as they came out, made sure that the terminology