# Coding Interview Preparation

The objective of this project is to teach you everything you need to know to get
a job (internship or full-time) as a Software Engineer at large companies such as
such as Microsoft, Google, Palantir, Facebook, Amazon and others.

The motivation is that famous resources such as Cracking the Coding Interview or
Leetcode are either becoming outdated or don't focus on the fundamental
knowledge as much as they should.

[Contributions are welcome!](https://gitlab.com/Igor1778/harvard-ai)
