# Gaurav Sen Mock System Design Interviews

### Design Instagram [2]

#### High level approach

- Store/get images (only obvious feature) (refer to Tinder video);
- Like/comment on image posts;
- Users following other users;
- Publish a news feed;

#### Clarifying questions

- Like/comment on image posts;
  - Can you comment on a comment? You can reply to a comment, but not reply to a
    reply on Instagram, for simplicity, let's assume you can only comment, not reply;
- Users following other users;
  - Who follows me?
  - Who are the people I follow?

#### Database

- Like/comment on image posts;
  - Three SQL tables: likes, posts and comments;
  - Likes table columns: `parent_id` (post or comment), `user_id`, `timestampt`,
    `active` (or deleted) and `type` (specify parent is comment or post);
  - Posts table columns: `post_id`, `user_id`, `text`, `image_url` and
    `timestamp`;
  - Query for number of likes on a post;
    - `select count (*) from likes where post_id="x"` would work but would be
      too slow considering a news feed;
    - We could add a column `likes` to the `posts` and `comments` tables and
      whenever a like is created/deleted, you update the number of likes in
      these tables using `parent_id`. This design is poor however, because
      information that is not relevant for the particular table is being stored
      there for convinience;
    - Instead, create an `activity` table with `activity_id` and `like_count`
      columns and store the `activity_id` in a column in the `likes` table;
  - Comments table columns: `comment_id`, `text` and `timestamp`;
  - Ids on these talbes are `UUID`;
  - Find the number of likes on this post queries the `activity` table;
  - Find all the comments on this post queries the `comments` table;
  - Find all users who have liked this post queries the `likes` table;
- Users following other users;
  - `follow` table columns: `follower_id`, `followee_id` and timestamp;
  - Find all users who follow a particular user makes a query with
    `followee_id` and returns a list of `follower_id`;
  - Find all users a particular user follows makes a query with
    `follower_id` and returns a list of `followee_id`;

#### Flow

- Instagream is mobile, so it needs a way to connect to server side. Assuming
  there is a gateway with a reverse proxy that decides where to send the request
  to and and encapsulates security mechanisms such as authentication tokens;
- The gateway can take external protocols such as http or xmpp (web sockets) and
  convert it to an internal protocol, for security reasons;
- Example of request: `get user feed with user_id`.
- User feed service: provides the top 20 posts for a user. To make it more
  robust, multiple servers can be used. Requests are routed to the server based
  on a load balancer;
- Load balancer: it is too expensive to ask the load balancer where to route on
  each request. Instead, the load balancer maintains a periodically updated
  snapshot of the system connected to the gateway to route the requests.
  Assuming load is balanced using consistent hashing (hash `user_id` to find the
  box to send the request);
- User feed service depends on the post service (all the posts for a particular
  day/user_id) and the follow service (all followers for a folowee and all
  followees to a follower);
- Image, activity, session, chat and profile services were skipped for simplicity;
- Get user feed for a `user_id`: use `getUsersFollowedBy(user_id)` to get a set
  of `user_id`s, and for each of them use `getPostsByUser(user_id)` in a for
  loop and add it to a set of `posts`. This set of `posts` is returned to the
  mobile app through the gateway;
- Optimization 1: batch process it by sending a set of `user_id`s and get the
  feed for each of them at once;
- Optimization 2: limit posts on news feed to 20 to reduce the load on the
  database;
- However, this is not going to scale. Recompute all the posts for every user
  request for the news feed is too expensive, especially on the post service
  because of the joins between tables. The solution is precompute the user news
  feed cached;
- How to precompute user feed? News feed is a set of posts made by people who
  this user follows. So everytime a person this user follows creates a post, the
  news feed of the user should be updated. When receiving a new post, the post
  services notifies the user feed service to update all precomputed user feeds
  of the users who follow the person who made this post. Use
  `getFollowers(user_id)` from the follow service and add the new post to each
  one's queue. Since the size of the queue is at most 20, it is pretty
  efficient.
- Should the precomputed news feed be stored in the database or in the cache? In
  the cache is better, because it separates the logic of the database from this
  data. In case there is a memory problem (server crashes, out of memory in
  cache for infrequent users), the news feed can be obtained in the
  "brute-force" way;
- Sending notification to followers when a new post is created is pretty easy.
  One can either use polling every, say, 10 seconds (expensive) or the server
  can push notifications with web sockets. If the person who made the post is a
  celebrity, it is possible to batch process the notifications (say every 100
  followers every 10 seconds) or wait for the users to get the notifications by
  polling. For celebrities it uses the poll mode, and for regular users it uses
  the push mode. Twitter, Quora, Facebook etc. maintain a queue or list where x
  posts a person has made to send to the followers;

### Design Whatsapp [3]

#### High level approach

- When listing start with simple features you know how to do, because the
  interviewer will probably agree with the first thing you say;
- Group messaging;
- Sent, delivered and read stamps;
- Last time online timestamp for users;
- Image sharing (Tinder video);
- Chats are temporary or permanent? Whatsapp stores messages on the user's phone
  to save storage space and give users more privacy;
- One to one chat;

#### Flow

_Sent, delivered and read stamps_

- With the app installed in the phone, you connect to whatsapp cloud through a
  gateway, which translates the external protocol to an internal protocol (does
  not need big headers and so much security internally);
- Person A sends a message to person B;
  - A user to box mapping is required, but it should not be stored on the
    gateway, since we want to maximize the number of TCP connections per gateway
    so using memory to keep this mapping is wasteful. Also, it is transient
    information, there would be lots of updates on this table. A better solution
    is create a sessions microservice that stores which user is connected to
    which box, with multiple servers to avoid single point of failure.
  - Person A connects to the gateway using `sendMessage(B)` function. The
    gateway just handles protocol change and sends the message to the session
    microservice. The session microservice finds the box user B is connected to
    and routes the message to this box, which then sends it to B.
  - Since the server (gateway) is sending a message to the client (user B),
    this can't be done using a client-to-server protocol like HTTP. It is
    possible to implement it with HTTP using long polling (every few minutes the
    client asks "hey, are the any new messages?"), but is not real-time and is
    expensive on client side. Instead, use Web Sockets over TCP, which is
    peer-to-peer communication, no client and server differentiation;
  - When A's message got to the gateway, a parallel response with the "sent"
    tick mark was sent to A, which means "this message will be delivered".
    Message is stored in a database and will keep retrying until B gets the
    message;
  - Once B gets the message, it sends another "notification" to its gateway to
    inform the message was received, TCP acknowledgment. This notification goes
    to the session microservice, and since the message contains `to` and `from`
    fields, a "received" tick mark is sent to user A;
  - The moment user B opens the app, a "read" notification is sent to user A
    through the same flow _Online/last seen_
- User B wants to know when was user A online last time.
  - Asking A would not be nice.
  - The "server" has a table with `user_id` and `last_seen`. Anytime user A
    reads, sends a message or does any kind of activity, this table is updated.
    If user A was online 3 seconds (arbitrary time interval) ago, it still shows
    as online to user B.
  - The "last seen" microservice should be able to differentiate automatic
    activities made by the app from user activities. Can be a flag in the
    message, that defines whether the "last seen" microservice table should be
    updated.
  - User B can just use the "last seen" microservice to know when was user A
    online last time.

_Group Messaging_

- Load balancing and authentication were used but are from other video;
- Profile, image, email and SMS services are present but are not key features of
  whatsapp.
- Group messaging
  - Red group and green group. One user can be in both groups. Users in the same
    group can be connected to different gateways;
  - It is too complicated for the session service to handle which user is part
    of which group. This was decoupled in a group service.
  - The session service when receiving a group message asks the group service
    "who are the other users in this group?", which responds with a list of
    `user_id`s. The session service goes through its database to find which box
    each of this users is connected to and routes the messages to the gateways
    one by one;
  - Since this is expensive, whatsapp gives a limit of 200 users per group.
    Because of the real-time requirement, poll and push mechanism of
    celebrity/regular user in instagram does not work.
  - Gateways will be starving for memory, so checking authentication and
    converting message to objects should be decoupled. One could use a
    parsed/unparsed microservice with two servers, which converts the external
    protocol to an internal one (like Thrift);
  - In group service there is a `group_id` to `user_id` mapping, so it could use
    consistent hashing to reduce duplication in the information across servers
    by delegating a specific information to a specific box. It should be routed
    based on the `group_id`;
  - Because the group microservice may fail, a message queue is used to ensure
    the message will be sent. Delay and how many retries are configurable. If
    the retry max count is reached, a failure message should be sent to the
    client.
  - In group messaging, a "sent" tick mark is sent to the user who created the
    message. However, "read" tick marks would be expensive (acknoledgment every
    member read) so most apps don't have that;
  - Having retrial, imdempotency and ordering is important in chat applications
    (check Tinder video);
  - In case of huge events, like New Year, messaging apps deprioritize messages.
    Instead of dropping them, features such as message delivered/seen tick marks
    are simply deativated and the message is just delivered. This allows the
    system to perform ok instead of not performing at all;

### Design Tinder [4]

#### High level approach

- When asked "design an app like Tinder" in an interview, instead of worrying
  about services/databases, take a step back and think about what kind of
  features you would need to provide this person.
- Go from front to back, starting from what the user needs as features, break it
  down into services and then detail the data requirements on each service;

#### Clarifying questions

- Are we going to store profiles with images, right?
- How many images per profile? Let's say 5 images;
- How are we going to recommend matches?
- How many active users?
- Which percentage of number of active users do a match? Assume 0.1% of total
  number of users;
- Avoid taking too many features. Four features is enough for one hour of
  interview;

#### Features

_Store Profiles_

- How we can store images? In this system there will be many of them. The two
  most common approaches are using `files` or using `blobs` (binary large
  object);
- Blob:
  - Structure specially designed to store large binary objects in databases;
  - Database does not really provide much more features to store images than
    files. Databases provide mutability (change a row in a database),
    transaction guarantees (ACID), indexes (improved search capability) and
    access control;
  - An update in an image usually will change the entire image, not just a few
    pixels. So, mutability is unnecessary;
  - Atomic operations are not going to be performed on images, so transaction
    guarantees are unnecessary;
  - For an image as blob, search capability is useless, because you will never
    search an image considering its binary content;
  - Access control is required and would be useful. However, it is possible to
    have equivalent access control mechanisms using a file system;
- File:
  - Cheaper storage of images;
  - Faster, because they store large objects separated from each other (more
    simple than vertical partitioning of a database, but has `select(*)`
    issues);
  - Static, so CDN (content delivery network) allows fast access. File URL from
    a distributed file system is stored together with `image_id` on the
    database;

#### Flow

_Store Profile and Images_

- Client on mobile application. User clicks a button and sends a request with
  `username` and `password` to the profile service, which checks its database
  and performs authentication. For simplicity, let's assume it is capable of
  sending emails for two-step authentication;
- Update profile: profile service generates a token, which is used to check if
  this update request comes from the user this profile belongs to;
- Having authentication with tokens inside the profile service is bad because if
  a new service is created in the future, it would have to communicate with the
  profile service on every authentication, generating duplicated code. Instead,
  create a gateway service which checks the token and directs the request to the
  correct service or fails the request. Benefits of this decoupling are
  separating internal and external protocols too.
- Profile service should store profile name and description. Images should
  probably be stored in a separate service, because a service in the future
  might only need the images. It has a Distributed File System to store images
  and a database with `profile_id`, `image_id` and `image_url`;

_Direct Messaging After Match_

- HTTP (Hypertext Transfer Protocol) is a protocol of communication between
  machines, where there is a client and a server. Requests always go from client
  to server, not the other way around. So, it is not possible to have direct
  messaging over HTTP, because even long polling is not really real-time and
  extremly inneficient;
- Instead, messages should be pushed to the mobile app using a peer-to-peer
  protocol (no client server distinction) such as XMPP WITH A Web Socket
  connection (or TCP, writing your own protocol);
- `connection_id` to `user_id` lookup should be stored in a session service to
  decoouple from the gateway;

_Noting Matches_

- Storing information to who a particular user matched to inside the mobile
  device has pros and cons. Cons are that the server is no longer the source of
  truth, so if the information is lost on client side, it cannot be rebuilt.
- Not to loose information about matched users, a matcher service should be
  used. The matcher service also checks if you a given user is allowed to send a
  direct message to another particular user.
- If the app is uninstalled, when it is installed again, the user would pull all
  the matches and be able to send direct messaging to these users. The only
  thing that would be lost is the swipes left or right (because they were only
  stored inside the mobile app), which is not critical information.

_Recommendations_

- Core of recommendation is geographic proximity, gender and age. These
  informations are stored inside the profile service.
- Why not put indices in all three? This is a misconception, because it is not
  possible to ahve the data sorted in multiple ways in a single table. If it is
  sorted by age,gender and location, any query would be able to use only one of
  these fields, which is responsibility of the index optimizer (out of your
  control);
- Solutions:
  - Use a NoSQL database like Cassandra or Amazon Dynamo which buids a table
    depending on the query;
  - Use SQL with sharding (horizontal partitioning), setting information for
    each column and directing data depending on the value of the field. For
    example, for the `name` field, starting `A-J` would go to database 36, `K-P`
    would go to database 79 and so on, so you query the right table based on the
    first letter of the name. To avoid single point of failure, a master-slave
    architecture can be used with servers with identical data;
- Shard the data based on the location of the user (could be a city, or chunks
  of it). This way, a recommendations service can easily pull other users within
  the same location chunk of the client and filter out based on gender and age;
- Client should push location updates to the server evey hour or so;

# References

[2] Gaurav Sen - Designing Instagram: System Design of News Feed, 2019.
<https://www.youtube.com/watch?v=QmX2NPkJTKg&list=PLMCXHnjXnTnvo6alSjVkgxV-VH6EPyvoX&index=21>

[3] Gaurav Sen - Whatsapp System Design: Chat Messaging Systems for Interviews, 2019.
<https://www.youtube.com/watch?v=vvhC64hQZMk&t=438s>

[4] Gaurav Sen - System Design: Tinder as a microservice architecture, 2019.
<https://www.youtube.com/watch?v=tndzLznxq40>
