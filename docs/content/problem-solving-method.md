# Problem Solving Methods for Coding Interviews

## Problem Solving Method by Cracking the Coding Interview [1]

1. Listen: hear the problem carefully and pay attention to any unique
   information to the problem, you probably need it all for the optimal
   solution. Take brief notes on the board if needed;
2. Example: if not provided, draw an example for the problem. The example should
   be: big enough and not a special case;
3. Brute force: get a brute force solution to the problem. Focus on correctness,
   but do not worry about special cases. Manually solving the example might
   help. State the time and space analysis. Don't code yet!;
4. Optimize: analyze your brute force solution looking for BUD: Bottlenecks,
   Unnecessary work and Duplicated work. For every optimization or new solution
   you make, state the Big-O time and space analysis. You know you are finished
   when you reach the Best Conceivable Time, but you might want to stop earlier
   if short on time (an inefficient solution is better than no solution):
   - Look for unused information;
   - Manually solve an example and reverse engineer your process. How did you
     solve it?
   - Solve it "incorrectly" and think about why the algorithm fails. Can you fix
     the issues?
   - Make a time v.s. space tradeoff. Hash tables can be very useful;
5. Walk Through: analyze your most optimal solution, manually solving examples.
   Look at how many variables are and when they change. Make sure you understand
   every detail before coding;
6. Implement: write down your code starting on the top left corner. Leave some
   blank space between lines to add more statements in the future if needed.
   Write it as beautiful as possible. Keep in mind what abbreviations you might
   want to make to save time;
7. Test: make a detailed code review of the written solution, thinking whether
   each line does what you think it does. Carefully fix bugs you might find,
   comprehending their cause. Keep in mind the follows:
   - Unusual or non-standard code;
   - Hot spots, like arithmetic, array iteration and null nodes;
   - Test with smaller test cases, to save time;
   - Consider special cases and edge cases;

## Problem Solving Method by CS Dojo [3]

1. Find a brute force solution by manually solving the problem;
2. Solve a simpler version of the problem and use the insight that you get to
   solve the original problem;
3. Think of the problem using simpler examples, which you can calculate by hand
   easily, and try to notice a pattern. The pattern might be clearer after
   making an operation on the input e.g. sorting;
4. Use some form of visualization, like very big examples (you don't have to
   calculate it), to validate the pattern. Use this pattern to develop a
   possible solution;
5. Test your solution with smaller examples until you are comfortable enough to
   code it. If you find a bug, either solve it or go back to step 2;

- State the Big O time and space analysis and ask the interviewer if you should
  look for a more optimal solution in every step;
- If you are not completely comfortable with your solution, propose to the
  interviewer writing some code. If he/she looks happy, write the code;

## Problem Solving Method by Vanshil Shah [5]

1. Repeat the problem to the interviewer in your own words: useful to make sure
   you understand the problem. Regurgitating the problem as stated doesn't
   achieve anything. The interviewer will correct your misunderstandings. This
   is where you confirm behaviour for edge cases and clarify any assumptions you
   make.
2. Provide a brute force solution: make sure it actually solves the problem and
   you provide the time complexity. This solution should come very quickly and
   it will do two things: A) confirm you understand the problem; B) give you a
   base solution to work off and optimize; If your solution doesn't work the
   interviewer will hint that something is wrong. Interviewers want you to
   succeed on the interview. Pay attention and catch any hints they sneak in.
3. Solve the problem as a human: our brains adapt for problem solving and we
   can often replicate our solutions with code. Usually when people are
   searching for pages in a book, there is an occurrence of our brains using
   binary search without even realizing it. Imagine the problem as if it was a
   riddle or brain teaser you had to solve. Pay attention to the strategies you
   develop, the way you hold onto information, and the calculations you make.
4. Solve some examples: involves understanding the problem and coming up with a
   human solution. Doing examples helps you understand commonalities between
   solutions to the problem. Observe how the solution relates to the inputs. Is
   there more than one solution? Are some solutions easier to calculate than
   others? These observations will lead to reduced time complexities in your
   solution.
5. Search your tool box of data structures and algorithms: think about how the
   structure of the data affects your solution. Think about how a data structure
   or algorithm could mitigate inefficiencies. You will likely discover
   optimizations before you exhaust your options.
6. Pitch the optimized solution (with time complexity): most people skip steps
   2, 3, 4, jump straight to 5 and try to pitch their best attempt at an
   optimized solution. This doesn't work because they didn't spend time
   understanding the problem. They become dissuaded from pitching another
   solution and spend a lot of time unsure what to do next. If you get here and
   still need to optimize. Go back and repeat steps 3-5. Remember to use
   recognizable terms when explaining, and be proactive in mentioning
   complexities.
7. Code: getting the right answer on step 6 doesn't mean you passed the
   interview. Getting the wrong answer on step 6 doesn't mean you failed the
   interview. The simplicity and elegance with which you can do this part will
   set you apart. One time during an interview I struggled heavily on optimizing
   the problem. In fact, I spent 35 minutes coming up with a solution. The
   interviewer might have even nudged me in the right direction. With 10 minutes
   left, he'd probably decided to not pass me on that interview. Yet, I managed
   to code the solution in 5 minutes. The interviewer wanted to make sure my
   solution worked on some examples. So we ran my code, debugged typos, and had
   a working solution we tested against for the remaining 5 minutes. I passed
   that interview.

# References

[1] McDowell, Gayle L. - Cracking the Coding Interview, 6th Edition, 2019.

[3] YK Sugi (CS Dojo) - 5 Problem Solving Tips for Cracking Coding Interview
Questions, 2019. <https://www.youtube.com/watch?v=GBuHSRDGZBY>

[5] Vanshil Shah - Coding Interviews Unravelled, 2019.
<https://vanshil.com/blog/coding_interviews_unravelled.html>
